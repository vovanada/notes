package queue

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestPriority(t *testing.T) {
	c := 100
	rand.Seed(time.Now().UTC().UnixNano())
	q := NewExpiredQueue()
	testTime := int64(1514764800)

	for i := 0; i < c; i++ {
		note := notes.Note{
			CreateTime:     testTime,
			ExpirationTime: rand.Int63n(60 * 24),
			Text:           fmt.Sprintf("test %v", i),
		}
		q.AddItem(note, "test@email.com")
	}

	prev := int64(0)

	for {
		item := q.Expired(time.Now().Unix())
		if item == nil {
			break
		}
		if prev == 0 {
			prev = item.priority
		}
		if prev > item.priority {
			t.Error("queue is not prioritized")
		}
	}
}
