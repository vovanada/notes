package queue

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"container/heap"
	"sync"
)

type ExpiredQueue struct {
	queue priorityQueue
	mu    sync.RWMutex
}

func NewExpiredQueue() *ExpiredQueue {
	pq := make(priorityQueue, 0)
	return &ExpiredQueue{
		queue: pq,
	}
}

func (eq *ExpiredQueue) Expired(time int64) *Item {
	eq.mu.Lock()
	defer eq.mu.Unlock()
	if eq.queue.Len() == 0 {
		return nil
	}
	if eq.queue.FirstPriority() < time {
		elem := heap.Pop(&eq.queue)
		return elem.(*Item)
	}
	return nil
}

func (eq *ExpiredQueue) AddItem(note notes.Note, userEmail string) {
	eq.mu.Lock()
	defer eq.mu.Unlock()
	item := &Item{
		value:     note,
		userEmail: userEmail,
		priority:  note.ExpirationTime + note.CreateTime,
	}
	heap.Push(&eq.queue, item)
}
