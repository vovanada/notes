package queue

import "bitbucket.org/vovanada/notes/api/src/app/notes"

type Item struct {
	value     notes.Note
	userEmail string
	priority  int64
	index     int
}

func (i Item) Value() notes.Note {
	return i.value
}

func (i Item) Email() string {
	return i.userEmail
}
