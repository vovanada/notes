package notes

type Note struct {
	ID             string `json:"id"`
	Text           string `json:"text"`
	CreateTime     int64  `json:"create_time"`
	ExpirationTime int64  `json:"expiration_time"`
}
