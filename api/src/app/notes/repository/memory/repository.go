package memory

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"bitbucket.org/vovanada/notes/api/src/app/notes/repository"
	"github.com/pborman/uuid"
	"sync"
	"time"
)

type memoryRepository struct {
	notes map[string][]notes.Note
	mu    sync.RWMutex
}

func NewNoteMemoryRepository() repository.NoteRepository {
	repo := &memoryRepository{}
	repo.notes = make(map[string][]notes.Note)
	return repo
}

func (r *memoryRepository) AddNote(userEmail string, note notes.Note) (notes.Note, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	note.ID = uuid.New()
	note.CreateTime = time.Now().Unix()
	r.notes[userEmail] = append(r.notes[userEmail], note)
	return note, nil
}

func (r *memoryRepository) EditNote(userEmail string, noteID string, text string) (notes.Note, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	for k := range r.notes[userEmail] {
		if r.notes[userEmail][k].ID == noteID {
			r.notes[userEmail][k].Text = text
			return r.notes[userEmail][k], nil
		}
	}
	return notes.Note{}, repository.ErrNotFound
}

func (r *memoryRepository) DeleteNote(userEmail string, noteID string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	for k := range r.notes[userEmail] {
		if r.notes[userEmail][k].ID == noteID {
			r.notes[userEmail] = append(r.notes[userEmail][:k], r.notes[userEmail][k+1:]...)
			return nil
		}
	}
	return repository.ErrNotFound
}
func (r *memoryRepository) DelayNote(userEmail string, noteID string) (notes.Note, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	for k := range r.notes[userEmail] {
		if r.notes[userEmail][k].ID == noteID {
			r.notes[userEmail][k].CreateTime = time.Now().Unix()
			return r.notes[userEmail][k], nil
		}
	}
	return notes.Note{}, repository.ErrNotFound
}
func (r *memoryRepository) GetNotes(userEmail string) ([]notes.Note, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	return r.notes[userEmail], nil
}
