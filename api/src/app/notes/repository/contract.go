package repository

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"errors"
)

var (
	ErrNotFound = errors.New("note not found")
)

type NoteRepository interface {
	AddNote(userEmail string, note notes.Note) (notes.Note, error)
	DeleteNote(userEmail string, noteID string) error
	DelayNote(userEmail string, noteID string) (notes.Note, error)
	EditNote(userEmail string, noteID string, text string) (notes.Note, error)
	GetNotes(userEmail string) ([]notes.Note, error)
}
