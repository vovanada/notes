package server

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"encoding/json"
	"errors"
)

const (
	LoginMessage      = "login"
	AddNoteMessage    = "add-note"
	EditNoteMessage   = "edit-note"
	DeleteNoteMessage = "delete-note"
	DelayNoteMessage  = "delay-note"
	NotesMessage      = "notes"
	ErrMessage        = "error"
)

type Message struct {
	Type string `json:"type"`
	Data json.RawMessage
}

type AddNoteMessageData struct {
	Text           string `json:"text"`
	ExpirationTime int64  `json:"expiration_time"`
}

func (m AddNoteMessageData) Validate() error {
	if len(m.Text) > 200 {
		return errors.New("limit is 200 characters")
	}

	if m.ExpirationTime <= 0 {
		return errors.New("time has to be more than a second")
	}

	return nil
}

type DeleteNoteMessageData struct {
	ID string `json:"id"`
}

type DelayNoteMessageData struct {
	ID string `json:"id"`
}

type EditNoteMessageData struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

type LoginMessageData struct {
	Email string `json:"email"`
}

type ExpiredNote struct {
	Type string `json:"type"`
	ID   string `json:"id"`
}

type SendNotes struct {
	Type         string       `json:"type"`
	Notes        []notes.Note `json:"notes"`
	ExpiredNotes []notes.Note `json:"expired_notes"`
}

type SendErr struct {
	Type  string `json:"type"`
	Error string `json:"error"`
}
