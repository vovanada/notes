package server

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes/repository"
	"bitbucket.org/vovanada/notes/api/src/app/queue"
	"log"
	"time"
)

type WebSocketServer struct {
	clients        map[*Client]struct{}
	broadcast      chan []byte
	register       chan *Client
	unregister     chan *Client
	emailClients   *emailClients
	noteRepository repository.NoteRepository
	expiredQueue   *queue.ExpiredQueue
}

func NewWebSocketServer(noteRepository repository.NoteRepository, expiredQueue *queue.ExpiredQueue) *WebSocketServer {
	return &WebSocketServer{
		broadcast:      make(chan []byte),
		register:       make(chan *Client),
		unregister:     make(chan *Client),
		clients:        make(map[*Client]struct{}),
		emailClients:   NewEmailClients(),
		noteRepository: noteRepository,
		expiredQueue:   expiredQueue,
	}
}

func (h *WebSocketServer) Run() {
	log.Printf("run socket server \n")
	go h.ExpiredMessages()
	for {
		select {
		case client := <-h.register:
			h.clients[client] = struct{}{}
		case client := <-h.unregister:
			log.Printf("%s unregistered", client.email)
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				h.emailClients.RemoveClient(client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
					log.Printf("%s\n", message)
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (h *WebSocketServer) ExpiredMessages() {
	for {
		time.Sleep(time.Second)
		for {
			item := h.expiredQueue.Expired(time.Now().Unix())
			if item == nil {
				break
			}

			for _, c := range h.emailClients.Clients(item.Email()) {
				c.UpdateNotes()
			}
		}
	}
}
