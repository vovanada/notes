package server

import "sync"

type emailClients struct {
	clients map[string][]*Client
	mu      sync.RWMutex
}

func NewEmailClients() *emailClients {
	return &emailClients{
		clients: make(map[string][]*Client),
	}
}

func (e *emailClients) AddClient(c *Client) {
	e.mu.Lock()
	defer e.mu.Unlock()
	e.clients[c.email] = append(e.clients[c.email], c)
}

func (e *emailClients) Clients(email string) []*Client {
	e.mu.RLock()
	defer e.mu.RUnlock()
	return e.clients[email]
}

func (e *emailClients) RemoveClient(c *Client) {
	e.mu.Lock()
	defer e.mu.Unlock()
	if _, ok := e.clients[c.email]; ok {
		for i := range e.clients[c.email] {
			if e.clients[c.email][i] == c {
				e.clients[c.email] = append(e.clients[c.email][:i], e.clients[c.email][i+1:]...)
			}
		}
	}
}
