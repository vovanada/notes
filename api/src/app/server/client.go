package server

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes"
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 2048
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  2024,
	WriteBufferSize: 2024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *WebSocketServer

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte

	email string
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		c.parseMessage(message)
		//c.hub.broadcast <- message
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func ServeWs(hub *WebSocketServer, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client

	go client.writePump()
	go client.readPump()
}

func (c *Client) parseMessage(messageStr []byte) {
	var m Message

	err := json.Unmarshal(messageStr, &m)

	if err != nil {
		c.SendError(err)
		return
	}

	switch m.Type {
	case LoginMessage:
		log.Printf("login")
		var data LoginMessageData
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			c.SendError(err)
			break
		}
		log.Printf("login data %s\n", m.Data)
		c.email = data.Email
		c.hub.emailClients.AddClient(c)
		err = c.UpdateNotes()
		if err != nil {
			c.SendError(err)
			break
		}

	case AddNoteMessage:
		log.Printf("add-note")
		var data AddNoteMessageData
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			c.SendError(err)
			break
		}
		if err := data.Validate(); err != nil {
			c.SendError(err)
			break
		}
		note, err := c.hub.noteRepository.AddNote(c.email, notes.Note{Text: data.Text, ExpirationTime: data.ExpirationTime})
		if err != nil {
			c.SendError(err)
			break
		}
		c.hub.expiredQueue.AddItem(note, c.email)

		err = c.UpdateNotes()
		if err != nil {
			c.SendError(err)
			break
		}

	case DeleteNoteMessage:
		log.Printf("delete-note")
		var data DeleteNoteMessageData
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			c.SendError(err)
			break
		}
		err = c.hub.noteRepository.DeleteNote(c.email, data.ID)
		if err != nil {
			c.SendError(err)
			break
		}

		err = c.UpdateNotes()
		if err != nil {
			c.SendError(err)
			break
		}

	case DelayNoteMessage:
		log.Printf("delay-note")
		var data DelayNoteMessageData
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			c.SendError(err)
			break
		}
		note, err := c.hub.noteRepository.DelayNote(c.email, data.ID)
		if err != nil {
			c.SendError(err)
			break
		}
		c.hub.expiredQueue.AddItem(note, c.email)

		err = c.UpdateNotes()
		if err != nil {
			c.SendError(err)
			break
		}

	case EditNoteMessage:
		log.Printf("edit-note")
		var data EditNoteMessageData
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			c.SendError(err)
			break
		}
		note, err := c.hub.noteRepository.EditNote(c.email, data.ID, data.Text)
		if err != nil {
			c.SendError(err)
			break
		}
		c.hub.expiredQueue.AddItem(note, c.email)

		err = c.UpdateNotes()
		if err != nil {
			c.SendError(err)
			break
		}
	}

	log.Printf("parse: %+v", m)
}

func (c *Client) UpdateNotes() error {
	allNotes, err := c.hub.noteRepository.GetNotes(c.email)

	if err != nil {
		return err
	}

	var currentNotes, expiredNotes []notes.Note

	currentTime := time.Now().Unix()

	for k := range allNotes {
		if allNotes[k].CreateTime+allNotes[k].ExpirationTime > currentTime {
			currentNotes = append(currentNotes, allNotes[k])
		} else {
			expiredNotes = append(expiredNotes, allNotes[k])
		}
	}

	sendMessage := SendNotes{
		Type:         NotesMessage,
		Notes:        currentNotes,
		ExpiredNotes: expiredNotes,
	}
	send, err := json.Marshal(sendMessage)
	if err != nil {
		return err
	}
	log.Printf("message: %+v", sendMessage)
	c.send <- send
	return nil
}

func (c *Client) SendError(err error) {

	sendMessage := SendErr{
		Type:  ErrMessage,
		Error: err.Error(),
	}
	send, err := json.Marshal(sendMessage)
	if err != nil {
		log.Printf("cant marshal err: %s", err)
	}
	c.send <- send
}
