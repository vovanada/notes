package main

import (
	"bitbucket.org/vovanada/notes/api/src/app/notes/repository/memory"
	"bitbucket.org/vovanada/notes/api/src/app/queue"
	"bitbucket.org/vovanada/notes/api/src/app/server"
	"flag"
	"log"
	"net/http"
)

var addr = flag.String("addr", ":1501", "http service address")

func main() {
	flag.Parse()
	hub := server.NewWebSocketServer(memory.NewNoteMemoryRepository(), queue.NewExpiredQueue())
	go hub.Run()
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		server.ServeWs(hub, w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
