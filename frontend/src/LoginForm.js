import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {email: '', password: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
        this.props.onSubmit(this.state.email, this.state.password);
        this.setState({value: '', time: ''});
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    <p>
                        Email:
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange}/>
                    </p>
                    <p>
                        Password:
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
                    </p>
                </label>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

LoginForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};