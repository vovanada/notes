import React, {Component} from 'react';
import './App.css';
import NoteForm from "./NoteForm";
import Modal from 'react-modal';
import LoginForm from "./LoginForm";
import NoteItem from "./NoteItem";

Modal.setAppElement(document.getElementById('root'));

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: [],
            modalIsOpen: true,
            expiredNotes: [],
            userEmail: "",
        };
        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        // this is an "echo" websocket service for testing pusposes
        this.connection = new WebSocket('ws://localhost:1501/ws');
        // listen to onmessage event
        this.connection.onmessage = evt => {
            let message = JSON.parse(evt.data);
            if (message.type === "notes") {
                this.setState({
                    notes: message.notes,
                    expiredNotes: message.expired_notes,
                })
            }
            if (message.type === "error") {
                alert(message.error);
            }
        };
    }

    addNote(value, time) {
        this.connection.send(JSON.stringify({
            type: "add-note", data: {"text": value, "expiration_time": parseInt(time, 10)}
        }));
    }

    editNote(id, value) {
        this.connection.send(JSON.stringify({
            type: "edit-note", data: {"text": value, "id": id}
        }));
    }

    login(email, password) {
        this.setState({
            "userEmail": email,
        });
        this.connection.send(JSON.stringify({
            type: "login", data: {"email": email}
        }));
    }

    deleteNote(data) {
        this.connection.send(JSON.stringify({type: "delete-note", data: {"id": data.target.value}}));
    }

    delayNote(data) {
        this.connection.send(JSON.stringify({type: "delay-note", data: {"id": data.target.value}}));
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {
        const {notes, expiredNotes} = this.state;
        let expiredModal = !(this.state.expiredNotes === null || this.state.expiredNotes.length === 0);
        let loginModal = this.state.userEmail === "";

        return (
            <div className="App">
                <Modal
                    isOpen={loginModal}
                    contentLabel="Login modal"
                >
                    <LoginForm onSubmit={this.login.bind(this)}/>
                </Modal>
                <Modal
                    isOpen={expiredModal}
                    contentLabel="Example Modal"
                    //onRequestClose={this.closeModal}
                >
                    {this.state.expiredNotes !== null &&
                    <div itemID="modal-expired-item">
                        <h1 id="heading">Expired notes</h1>
                        <div id="full_description">
                            <ul className="App-ui">
                                {expiredNotes.map(note => (
                                    <li key={note.id}>
                                        {note.text}
                                        <p>
                                            <button value={note.id} onClick={this.deleteNote.bind(this)}>delete</button>
                                            <button value={note.id} onClick={this.delayNote.bind(this)}>delay</button>
                                        </p>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                    }
                </Modal>
                <header className="App-header">
                    <h1 className="App-title">Notes</h1>
                </header>
                <NoteForm onSubmit={this.addNote.bind(this)}/>
                <div>

                    Notes:
                    {notes !== null && notes.length > 0 ?
                        <ul className="App-ui">
                            {notes.map(note => (
                                <NoteItem
                                    onDelete={this.deleteNote.bind(this)}
                                    onEdit={this.editNote.bind(this)}
                                    value={note.text}
                                    id={note.id}
                                    key={note.id}
                                />
                            ))}
                        </ul>
                        :
                        <p>Add first note</p>
                    }
                </div>
            </div>
        );
    }
}

export default App;
