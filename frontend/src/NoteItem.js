import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class NoteItem extends Component {
    constructor(props) {
        super(props);
        this.state = {value: this.props.value, id: this.props.id};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
        this.props.onSubmit(this.state.value, this.state.time);
        this.setState({
            value: '',
            time: '',
            showEdit: false,
        });
        event.preventDefault();
    }

    delete(event) {
        this.props.onDelete(event);
        event.preventDefault();
    }

    edit(event) {
        this.props.onEdit(this.state.id, this.state.value);
        this.triggerShowEdit();
        event.preventDefault();
    }

    triggerShowEdit() {
        this.setState({
            "showEdit": !this.state.showEdit,
        })
    }

    render() {
        return (
            <li>
                {this.state.showEdit ?
                    <textarea name="value" value={this.state.value} onChange={this.handleChange}></textarea>
                    :
                    <p>{this.state.value}</p>
                }

                <p>
                    <button value={this.state.id} onClick={this.delete.bind(this)}>delete</button>
                    {this.state.showEdit ?
                        <button value={this.state.id} onClick={this.edit.bind(this)}>save</button>
                        :
                        <button value={this.state.id} onClick={this.triggerShowEdit.bind(this)}>edit</button>
                    }
                </p>
            </li>
        );
    }
}

NoteItem.propTypes = {
    onDelete: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
};