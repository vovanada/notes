import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class NoteForm extends Component {
    constructor(props) {
        super(props);
        this.state = {value: '', time: '', error:''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value, error:''});
    }

    handleSubmit(event) {
        if (this.state.time <= 0 || this.state.value.length > 200) {
            this.setState({error: 'incorrect data'});
        } else {
            this.props.onSubmit(this.state.value, this.state.time);
            this.setState({value: '', time: ''});
        }
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <p>
                    Text:<br/>
                    <textarea type="text" name="value" value={this.state.value} onChange={this.handleChange}/>
                </p>
                <p>
                    Live time (in sec):&nbsp;
                    <input type="text" name="time" value={this.state.time} onChange={this.handleChange}/>
                </p>
                <p>{this.state.error}</p>
                <input type="submit" value="Add"/>
            </form>
        );
    }
}

NoteForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};