* Run `docker-compose up`
* Open `localhost:1500`

####Possible improvements
* Add handling error when socket disconnect and try to reconnect
* Add linter and handle all socket errors
* Save email in local storage and don't ask after page reloading

